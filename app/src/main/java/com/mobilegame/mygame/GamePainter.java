package com.mobilegame.mygame;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.content.Context ;
import android.graphics.Canvas ;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

// Рисование игры
public class GamePainter extends View {

    private Game game ;

    // Текстуры
    private Bitmap Hero ;
    private Bitmap Monster ;
    private Bitmap Bull ;

    public GamePainter(Context context, Game Agame) {
        super(context);

        game = Agame ;

        // Установка текстур из ресурсов
        Resources res = this.getResources();
        Monster = BitmapFactory.decodeResource(res, R.drawable.monster);
        Hero = BitmapFactory.decodeResource(res, R.drawable.hero);
        Bull = BitmapFactory.decodeResource(res, R.drawable.bull);
    }

    private Paint mPaint = new Paint();

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

        // Рисование на канве
        int W = this.getWidth() ;
        int H = this.getHeight() ;

        game.setScreen(W,H);

        // стиль Заливка
        mPaint.setStyle(Paint.Style.FILL);

        // закрашиваем холст белым цветом
        mPaint.setColor(Color.WHITE);
        canvas.drawPaint(mPaint);

        // Вывод информации
        mPaint.setColor(Color.BLUE);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true);
        mPaint.setTextSize(40);
        canvas.drawText("Сбито "+Integer.toString(game.getKillCount())+
                ",  время в игре: "+game.getGameTime()+
                ",  жизней: "+Integer.toString(game.getLifecount()), 30, 35, mPaint);

        // Вывод врагов
        for(Monster m:game.monsters)
            canvas.drawBitmap(Monster,(float)m.getX()-Monster.getWidth()/2,
                    (float)m.getY()-Monster.getHeight()/2,mPaint);
        // Вывод снарядов
        for(Bull b:game.bulls)
            canvas.drawBitmap(Bull,(float)b.getX()-Bull.getWidth()/2,
                    (float)b.getY()-Bull.getHeight()/2,mPaint);
        // Вывод игрока
        canvas.drawBitmap(Hero,W/2-Hero.getWidth()/2,H/2-Hero.getHeight()/2,mPaint);
    }

}
