package com.mobilegame.mygame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    private Game game ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Главное меню");
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Для опускания пальца или мыши на экран
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            // посылаем это игре
            game.sendClickAt(event.getX(),event.getY()) ;
        return true;
    }

    public void onButtonStartClick(View view)
    {
        // Создаем игру
        game = new Game() ;
        // Создаем рисователь игры
        GamePainter gp = new GamePainter(this, game);
        // Устанавливаем на него событие нажатия
        gp.setOnTouchListener(this);
        // Устанавливаем рисователь как контент действия
        setContentView(gp) ;
        this.setTitle("Игра начата");
        // Запуск обновления игры
        new GameUpdater(game, gp).start() ;
    }

    public void onButtonExitClick(View view) {
        // Выход
        finish();
    }


}
