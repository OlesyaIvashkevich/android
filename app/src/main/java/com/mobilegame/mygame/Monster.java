package com.mobilegame.mygame;

// Класс врага
public class Monster {
    // Координаты
    private double x ;
    private double y ;
    // Компоненты скорости
    private double vx ;
    private double vy ;

    public Monster(int Ax, int Ay, int Axc, int Ayc) {
        x = Ax ;
        y = Ay ;
        double xc = Axc;
        double yc = Ayc;

        // Расчет компонент скорости
        final double R = Math.sqrt((x- xc)*(x- xc)+(y- yc)*(y- yc)) ;
        vx = Game.MONSTER_SPEED*(xc -x)/R ;
        vy = Game.MONSTER_SPEED*(yc -y)/R ;
    }

    // Обновление
    public void update(double dt) {
        x+=vx*dt ;
        y+=vy*dt ;
    }

    // Пересечение
    public boolean isCrossedWith(int tx, int ty, int tr) {
        return (Math.sqrt((x-tx)*(x-tx)+(y-ty)*(y-ty))<=Game.MONSTER_SIZE+tr) ;
    }

    public double getX() {
        return x ;
    }

    public double getY() {
        return y ;
    }

}
