package com.mobilegame.mygame;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

// Класс игры
public class Game {

    // Время игры
    private double gameTime;
    // Счетчики жизней и очков
    private int lifeCount ;
    private int killCount ;
    // Счетчик врагов
    private double monsterCountDown ;
    // Генератор
    private Random rnd ;
    // Уровень
    private int level ;

    // Константы игры
    public final static int INITIAL_LIFE = 3 ;
    public final static int MONSTER_SIZE = 30 ;
    public final static int PLAYER_SIZE = 30 ;
    public final static int BULL_SIZE = 15 ;
    public final static int MONSTER_SPEED = 100 ;
    public final static int BULL_SPEED = 300 ;
    public final static double MONSTER_MIN_SEC = 3 ;
    public final static double MONSTER_MAX_SEC = 6 ;

    // Массивы врагов и пуль
    public List<Monster> monsters ;
    public List<Bull> bulls ;
    private int W ;
    private int H ;

    public Game() {
        gameTime = 0 ;
        lifeCount = INITIAL_LIFE ;
        killCount = 0 ;
        monsters = new ArrayList<Monster>() ;
        bulls = new ArrayList<Bull>() ;
        rnd=new Random() ;
        resetMonsterCountDown() ;
        level = 1 ;
    }

    // Установка ширины экрана
    public void setScreen(int AW, int AH) {
        W=AW ;
        H=AH ;
    }

    // Установка отсчета для врагов
    private void resetMonsterCountDown() {
        monsterCountDown = MONSTER_MIN_SEC+(MONSTER_MAX_SEC-MONSTER_MIN_SEC)*rnd.nextDouble()  ;
    }

    // Генерация новых врагов с разных сторон экрана
    private void newMonster() {
        for (int n=0; n<level; n++) {
            final int walln = rnd.nextInt(4);
            int x = rnd.nextInt(W);
            int y = rnd.nextInt(H);
            if (walln == 0) x = 0;
            if (walln == 1) y = 0;
            if (walln == 2) x = W;
            if (walln == 3) y = H;
            monsters.add(new Monster(x, y, W / 2, H / 2));
        }
    }

    // Обновление игры
    public void update(double dt) {
        if (isOver()) return ;

        // Повышение уровня
        if ((int)(gameTime / 20) != (int)((gameTime +dt)/20)) level++ ;

        // Общее время
        gameTime +=dt ;

        // Обновление объектов
        for (Monster m:monsters)
            m.update(dt);
        for (Bull b:bulls)
            b.update(dt);

        // Враги добегающие до игрока
        int p = 0 ;
        while (p<monsters.size())
            if (monsters.get(p).isCrossedWith(W/2,H/2,PLAYER_SIZE)) {
                lifeCount-- ;
                monsters.remove(p) ;
            }
            else
                p++ ;

        // Пули, вылетающие за край экрана
        p = 0 ;
        while (p<bulls.size())
            if (bulls.get(p).isOutOfScreen(W,H))
                bulls.remove(p) ;
            else
                p++ ;

        // Сбитие врагов пулями
        out:
        for (int m=0; m<monsters.size(); m++)
            for (int b=0; b<bulls.size(); b++)
                if (bulls.get(b).isCrossedWith((int)monsters.get(m).getX(),(int)monsters.get(m).getY(),MONSTER_SIZE)) {
                    monsters.remove(m) ;
                    bulls.remove(b) ;
                    killCount++ ;
                    continue out ;
                }

        // Отсчет для появления новых монстров
        monsterCountDown-=dt ;
        if (monsterCountDown<=0) {
            resetMonsterCountDown() ;
            newMonster();
        }
    }

    // Нажатие на экран - новая пуля
    public void sendClickAt(double x, double y) {
        if (Math.sqrt((x-W/2)*(x-W/2)+(y-H/2)*(y-H/2))<PLAYER_SIZE) return ;

        bulls.add(new Bull(W/2,H/2,(int)x,(int)y)) ;
    }

    public int getKillCount() {
        return killCount ;
    }
    public int getLifecount() {
        return lifeCount ;
    }
    public boolean isOver() {
        return lifeCount<=0 ;
    }
    // Перевод секунд в представление mm:ss - наиболее общим способом
    public String getGameTime() {
        long itemLong = (long) (gameTime * 1000);
        Date itemDate = new Date(itemLong);
        return new SimpleDateFormat("mm:ss").format(itemDate);
    }

}
