package com.mobilegame.mygame;

// Класс пули
public class Bull {
    // Координаты
    private double x ;
    private double y ;
    // Скорость
    private double vx ;
    private double vy ;

    public Bull(int Ax, int Ay, int Axc, int Ayc) {
        x = Ax ;
        y = Ay ;

        // Расчет компонент скорости
        final double R = Math.sqrt((x- (double) Axc)*(x- (double) Axc)+(y- (double) Ayc)*(y- (double) Ayc)) ;
        vx = Game.BULL_SPEED*((double) Axc -x)/R ;
        vy = Game.BULL_SPEED*((double) Ayc -y)/R ;
    }

    // Обновление
    public void update(double dt) {
        x+=vx*dt ;
        y+=vy*dt ;
    }

    // Пересечение
    public boolean isCrossedWith(int tx, int ty, int tr) {
        return (Math.sqrt((x-tx)*(x-tx)+(y-ty)*(y-ty))<=Game.BULL_SIZE+tr) ;
    }

    // Выход за пределы экрана
    public boolean isOutOfScreen(int W, int H) {
        return (x<0)||(y<0)||(x>W)||(y>H) ;
    }

    public double getX() {
        return x ;
    }

    public double getY() {
        return y ;
    }

}
