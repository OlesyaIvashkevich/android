package com.mobilegame.mygame;

import android.app.Activity;
import android.widget.Button;

import java.util.Timer;
import java.util.TimerTask;

// Служебный класс обновления игры
public class GameUpdater extends TimerTask {
    private final int INTERVAL = 100 ;
    private GamePainter gp ;
    private Game game ;
    private Timer timer ;

    public GameUpdater(Game Agame, GamePainter Agp) {
        game = Agame ;
        gp = Agp ;
    }

    // Запуск обновления через установку таймера на самого себя
    public void start() {
        timer = new Timer() ;
        timer.schedule(this,INTERVAL,INTERVAL);
    }

    @Override
    public void run() {
        // Запускаем действие в потоке интерфейса
        ((Activity)gp.getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Обновляем игру
                game.update((double)INTERVAL/1000);
                // Обновляем форму
                gp.invalidate();
                // Если игра окончена
                if (game.isOver()) {
                    timer.cancel();
                    Activity A = (Activity) gp.getContext() ;
                    A.setTitle("Игра окончена, набрано "+Integer.toString(game.getKillCount())+" очков");
                    A.setContentView(R.layout.activity_main);
                    ((Button)A.findViewById(R.id.button_start)).setText("Играть снова") ;
                    ((Button)A.findViewById(R.id.button_exit)).setWidth(
                            ((Button)A.findViewById(R.id.button_start)).getWidth()
                    );
                }
            }
        }) ;
    }

}
